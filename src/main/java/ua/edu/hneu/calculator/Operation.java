package ua.edu.hneu.calculator;

public enum Operation {
    ADDITION(1, "Sum"),
    SUBTRACTION(2, "Difference"),
    MULTIPLICATION(3, "Product"),
    DIVISION(4, "Division result"),
    GCD(5, "GCD"),
    LCM(6, "LCM"),
    EXIT(7, null),
    DO_NOTHING(8, null);

    private final int id;
    private final String humanRead;

    Operation(int id, String humanRead) {
        this.id = id;
        this.humanRead = humanRead;
    }

    public static Operation getById(int id) {
        for (var value : Operation.values()) {
            if (value.id == id) {
                return value;
            }
        }
        return DO_NOTHING;
    }

    public String getHumanRead() {
        return this.humanRead;
    }
}
